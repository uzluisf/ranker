#!/usr/bin env perl6
use Ranker;

my @scores = 44, 42, 42, 41, 41, 41, 39;

# Get Ranking object
my $rankings = Ranker::rank(@scores, strategy => 'fractional');

# Use Whatever subscript to grab all Ranking objects
my @ranking-objs = $rankings[*];

for @ranking-objs -> $r {
    put $r.score, " ", $r.rank;
}
