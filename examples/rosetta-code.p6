#!/usr/bin env perl6
use Ranker;

# Source: https://rosettacode.org/wiki/Ranking_methods

my %scores =
    Solomon => 44,
    Jason   => 42,
    Errol   => 42,
    Garry   => 41,
    Bernard => 41,
    Barry   => 41,
    Stephen => 39;

for <standard modified dense ordinal fractional> -> $s {
    my $rankings = Ranker::rank(%scores.values, :strategy($s));
    do-printing($s, $rankings);
}

sub do-printing( $s, $rankings ) {

    my %score-to-people;
    for %scores -> $p {
        %score-to-people{ $p.value }.push: $p.key;
    }

    put "{$s.uc}:\n";
    print q:to/END/;
    Name     Score Rank
    ----     ----- ----
    END
    

    my @score-seen;
    for $rankings[*] -> $ranking {
        next if $ranking.score ∈ @score-seen;
        @score-seen.push: $ranking.score;
        for %score-to-people{ $ranking.score }.flat -> $person {
            put "%-8s %-5s %-2s".sprintf($person, $ranking.score, $ranking.rank);
        }
    }
    put "";

}
