#| Ranking class which represents a particular ranking of values.
unit class Ranker::Ranking is export;

has $.rankings; #= The Rankings object storing all the Ranking objects
has $.index;
has $.rank;
has $.score;
has @.rankables;

# Public methods

#|«
Return the number of values (or rankables) in this particular ranking.
Same as self.rankables.elems where self is a Ranking object.
»
method rankables-num( --> Int:D ) {
    @!rankables.elems
}

#| Return the Ranking object's percentile.
method percentile {
    (self!num-scores-at-or-below / $!rankings.scores-num) * 100
}

#| Return the Ranking object's z-score.
method z-score {
    $!rankings.standard-dev == 0
        ?? 0.0
        !! ($!score - $!rankings.mean) / $!rankings.standard-dev;
}

# Private methods

method !num-scores-at-or-below {
    $!rankings[$!index .. $!rankings.scores-num]
    .grep(* !=== Nil).map(*.rankables-num).sum
}
