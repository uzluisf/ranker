unit class X::Ranker::Error is export;
also is Exception;

has Str $.what;
has     %.errors;

method new($what, %errors) {
    self.bless: :$what, :%errors;
}

method message {
    my $message = "$!what has errors: \n";
    $message   ~= %!errors.map({"→ {$_.key} {$_.value}"}).join("\n");
    return $message;
}
