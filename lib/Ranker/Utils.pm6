unit module Ranker::Utils;

#|«
Merge the elements from %second into %first. Those elements already
present in %first will be overwritten with those from %second. This doesn't
perform merging of deep hashes; just first level.
»
sub merge( %first, %second ) is export {
    my %new;

    for %second.keys -> $k {
        if %first{$k}:exists {
            %new{$k} = %second{$k};
            next;
        }
        %new{$k} = %second{$k};
    }

    for %first.keys -> $k {
        %new{$k} = %first{$k} unless %new{$k}:exists
    }

    return %new;
}
