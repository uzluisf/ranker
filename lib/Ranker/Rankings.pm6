use Ranker::Ranking;
use Ranker::Error;

#| Rankings class that stores indexable Ranking objects.
unit class Ranker::Rankings is export;

# Private attributes
has           @!scores;
has Ranking:D @!rankings;
has Int:D     $!index = 0;
has           %!errors;

#|«
Return the mean (sum of all scores / number of scores).
Same as self.total / self.scores-num.
»
method mean {
    Error.new('Rankings', %!errors).throw unless self!scores-are-valid;
    self.total / self.scores-num;
}

# Return the number of scores.
method scores-num( --> Int ) {
    @!scores.elems
}

#| Return the total sum of scores.
method total {
    @!scores.sum
}

#| Return the total difference.
method total-diff {
    @!scores.map((* - self.mean) ** 2).sum
}

#| Return the variance.
method variance {
    self.total-diff / @!scores.elems
}

#| Return the standard deviation among the scores.
method standard-dev {
    self.variance.sqrt;
}

#| Create a Ranking object and return it.
method create-ranking( $rank, $score, @rankables --> Ranking:D ) {
    @!scores.append: Array.new: $score xx @rankables.elems;

    # instantiate a Ranking object with the given data
    my Ranking $ranking .= new(
        :rankings(self), :index(self.elems), :$rank, :$score, :@rankables
    );

    @!rankings.push: $ranking;

    return $ranking;
}

#|«
Return the score value mapped to its rank(s). If the value isn't ranked,
note it and return Nil.
»
method rank( $score ) {
    my @scores = @!rankings».score.squish: :with(&infix:<eq>);

    # Make sure $score's been ranked.
    #say $score cmp @scores.any;
    unless $score cmp @scores.any {
        note "The value $score isn't ranked.";
        return Nil;
    }

    return $score => @!rankings.grep(*.score cmp $score === Same).map(*.rank);
}

#| Generate only the ranks.
method ranks {
    @!rankings».rank
}

# PRIVATE methods:

method !scores-are-valid( --> Bool ) {
    self!validate;
    return %!errors.elems == 0;
}

method !validate( --> Nil ) {
    %!errors = Empty;

    # check if scores have Any values
    my $has-Any-values = @!scores.grep(* === Any) ?? True !! False;
    %!errors<scores> = 'contains Any values' if $has-Any-values;
}

# OVERLOADED methods for positional subscripting

# Return number of Ranking objects in the class.
multi method elems( ::?CLASS:D: ) {
    return @!rankings.elems
}

# Return Ranking object at index. Otherwise, Nil.
multi method AT-POS( ::?CLASS:D: Int:D $index ) {
    return @!rankings[$index] if $index < @!rankings;
    return Nil;
}
