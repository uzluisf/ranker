unit module Ranker::Strategies;

use Ranker::Utils;
use Ranker::Ranking;
use Ranker::Rankings;
use Ranker::Error;


#| Role that acts as a base class for the different ranking strategies.
role Strategy is export {

    # PUBLIC attributes:

    has @.rankables; #= Values to be ranked
    has %.options;   #= key-value pair options

    # PRIVATE attributes:

    has %!errors;
    has Rankings $!rankings .= new;

    # SETUP:

    my %default-options := %(
        :by({ $^rankable }),
        :asc(False),
    );

    submethod TWEAK {
        %!options = %!options
            ?? merge(%default-options, %!options)
            !! %default-options;
    }

    # PUBLIC methods:

    #|«
    Group rankables in a hash such that each rankable value is paired up with
    an array containing the elements the block in %!options<by> output
    for that rankable value.
    »
    method rankables-grouped-by-score() returns Hash {
        @!rankables.classify: %!options<by>
    }

    #| Get the rankables for a given score.
    method rankables-for-score( $strategy: $score ) returns Seq {
        $strategy.rankables-grouped-by-score{$score}.flat
    }

    #|«
    Sort values in ascending if asc => True. Otherwise, use descending order.
    »
    method scores-unique-sorted( $strategy: ) returns Seq {
        # If asc is True, list is in ascending order.
        if %!options<asc> {
            $strategy.rankables-grouped-by-score.keys.sort
        }
        # Otherwise, descending order. This is the default.
        else {
            $strategy.rankables-grouped-by-score.keys.sort.reverse
        }
    }

    #| Return a Ranking object for the given values.
    method create-ranking( $rank, $score, @rankables ) returns Ranking {
        $!rankings.create-ranking: $rank, $score, @rankables
    }

    #| Perform appropriate strategy ranking.
    method rank( $strategy: ) returns Rankings {
        Error.new('Strategy', %!errors).throw unless $strategy!scores-are-valid;

        $strategy.execute();  # apply ranking strategy
        return $!rankings;    # return Rankings object
    }

    # Must be implemented by the class that composes this role.
    method execute() { ... }

    # PRIVATE methods:

    method !scores-are-valid( --> Bool ) {
        self!validate;
        return %!errors.elems == 0;
    }

    method !validate( --> Nil ) {
        %!errors = Empty;

        # check if scores have Any values
        my @scores = self.rankables-grouped-by-score.keys;
        my $has-Any-values = @scores.grep(* === Any) ?? True !! False;

        if $has-Any-values {
            %!errors<scores> = 'contains Any values'
        }
    }

}

# Class that implements the standard-competition ranking.
class StandardCompetition is export(:standard) {
    also does Strategy;

    # Perform the standard-competition ranking.
    method execute( $strategy: ) {
        my $rank = 1;
        for $strategy.scores-unique-sorted.values -> $score {
            my @rankables = $strategy.rankables-for-score: $score;
            $strategy.create-ranking: $rank, $score, @rankables;
            $rank += @rankables.elems;
        }
    }

}

# Class that implements the modified-competition ranking.
class ModifiedCompetition is export(:modified) {
    also does Strategy;

    # Perform the modified-competition ranking.
    method execute( $strategy: ) {
        my $rank = 0;
        for $strategy.scores-unique-sorted.values -> $score {
            my @rankables = $strategy.rankables-for-score: $score;
            if $rank == 0 {
                $strategy.create-ranking: 1, $score, @rankables;
                $rank += @rankables.elems;
            }
            else {
                $rank += @rankables.elems;
                $strategy.create-ranking: $rank, $score, @rankables;
            }
        }
    }
}

# Class that implements the dense ranking.
class Dense is export(:dense) {
    also does Strategy;

    # Perform the dense ranking.
    method execute( $strategy: ) {
        for $strategy.scores-unique-sorted.kv -> $index, $score {
            my $rank = $index + 1;
            my @rankables = $strategy.rankables-for-score: $score;
            $strategy.create-ranking: $rank, $score, @rankables;
        }
    }
}

# Class that implements the ordinal ranking.
class Ordinal is export(:ordinal) {
    also does Strategy;

    # Perform the ordinal ranking.
    method execute( $strategy: ) {
        my $rank = 1;
        for $strategy.scores-unique-sorted.values -> $score {
            my @rankables = $strategy.rankables-for-score: $score;
            for @rankables -> $rankable {
                $strategy.create-ranking: $rank, $score, [$rankable];
                $rank += 1;
            }
        }
    }
}

# Class that implements the fractional ranking.
class Fractional is export(:fractional) {
    also does Strategy;

    # Perform the fractional ranking.
    method execute( $strategy: ) {
        my $ord-rank = 1;

        for $strategy.scores-unique-sorted.values -> $score {
            # Get each score's ordinal rank(s)
            # (e.g., score => [rank, ranks, ...]).
            my %value2ranks;
            my @rankables = $strategy.rankables-for-score: $score;
            for @rankables -> $rankable {
                %value2ranks{ $rankable }.push: $ord-rank;
                $ord-rank += 1;
            }

            # Convert from ordinal to fractional ranks for each score. A
            # fractional rank for a score is the average of its ordinal ranks.
            my $sum   = %value2ranks{$score}.sum;
            my $elems = %value2ranks{$score}.elems;
            my @ranks = ($sum / $elems) xx $elems;

            # Create Ranking object for each fractional rank and score.
            $strategy.create-ranking: $_, $score, [$score] for @ranks;
        }
    }

}
