unit module Ranker;

use Ranker::Utils;
use Ranker::Ranking;
use Ranker::Rankings;
use Ranker::Strategies;

my $strategies := %(
    standard    => ::("Ranker::Strategies::StandardCompetition"),
    modified    => ::("Ranker::Strategies::ModifiedCompetition"),
    dense       => ::("Ranker::Strategies::Dense"),
    ordinal     => ::("Ranker::Strategies::Ordinal"),
    fractional  => ::("Ranker::Strategies::Fractional"),
);

my $default-options := %(
    by       => -> $x { $x },
    asc      => False,
    strategy => 'standard'
);

sub get-strategy( @rankables, %options ) {
    given %options<strategy> {
        # If strategy is class object composing the Strategy role,
        # return an instance of it.
        when Strategy:U {
            return $_.new(:@rankables, :%options)
        }

        # If strategy is a string representing one of the default strategies,
        # return an instance of the corresponding default strategy.
        when $strategies{$_}:exists {
            return $strategies{$_}.new(:@rankables, :%options)
        }

        default {
            die "Unknown strategy: {%options<strategy>}"
        }
    }

}

=begin comment
Return a Rankings object with values (or rankables) using the given options.

* @rankables contains the values to be ranked (or rankables).
* %options might contain different versions of the default options:
    - 'by' for custom ranking, e.g., by => { $^score }.
    - 'asc' for specifying ascending order. By default, descending order is used.
    - 'strategy' for specifying the ranking strategy. By default, 'standard'.
=end comment

our sub rank( @rankables, *%options ) returns Rankings {
    my %new-options = %options
        ?? merge($default-options, %options)
        !! $default-options;

    # get Strategy instance object
    my $strategy = get-strategy(@rankables, %new-options);
    
    return $strategy.rank;
}
