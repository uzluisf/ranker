use v6.c;
use Test;
use Ranker::Strategies :dense;

subtest 'when list of rankables is large' => sub {
    my @rankables = 1, 1, 2, 3, 3, 4, 5, 6, 7, 7, 7, 1, 1, 3;
    my $rankings = Dense.new(:@rankables).rank;

    cmp-ok $rankings.elems, &[==], 7, 'correct number of rankings';

    subtest 'first ranking' => sub {
        my $ranking = $rankings[0];
        cmp-ok $ranking.rank, &[==], 1, 'correct rank';
        is-deeply $ranking.rankables, [7, 7, 7], 'correct rankable values';
    }

    subtest 'second ranking' => sub {
        my $ranking = $rankings[1];
        cmp-ok $ranking.rank, &[==], 2, 'correct rank';
        is-deeply $ranking.rankables, [6], 'correct rankable values';
    }

    subtest 'third ranking' => sub {
        my $ranking = $rankings[2];
        cmp-ok $ranking.rank, &[==], 3, 'correct rank';
        is-deeply $ranking.rankables, [5], 'correct rankable values';
    }

    subtest 'fourth ranking' => sub {
        my $ranking = $rankings[3];
        cmp-ok $ranking.rank, &[==], 4, 'correct rank';
        is-deeply $ranking.rankables, [4], 'correct rankable values';
    }

    subtest 'fifth ranking' => sub {
        my $ranking = $rankings[4];
        cmp-ok $ranking.rank, &[==], 5, 'correct rank';
        is-deeply $ranking.rankables, [3, 3, 3], 'correct rankable values';
    }

    subtest 'sixth ranking' => sub {
        my $ranking = $rankings[5];
        cmp-ok $ranking.rank, &[==], 6, 'correct rank';
        is-deeply $ranking.rankables, [2], 'correct rankable values';
    }

    subtest 'seventh ranking' => sub {
        my $ranking = $rankings[6];
        cmp-ok $ranking.rank, &[==], 7, 'correct rank';
        is-deeply $ranking.rankables, [1, 1, 1, 1], 'correct rankable values';
    }
}

subtest 'when list of rankables is small' => sub {
    my @rankables = 3, 2, 2, 1;
    my $rankings = Dense.new(:@rankables).rank;

    cmp-ok $rankings.elems, &[==], 3, 'correct number of rankings';

    subtest 'first ranking' => sub {
        my $ranking = $rankings[0];
        cmp-ok $ranking.rank, &[==], 1, 'correct rank';
        is-deeply $ranking.rankables, [3], 'correct rankable values';
    }

    subtest 'second ranking' => sub {
        my $ranking = $rankings[1];
        cmp-ok $ranking.rank, &[==], 2, 'correct rank';
        is-deeply $ranking.rankables, [2, 2], 'correct rankable values';
    }

    subtest 'third ranking' => sub {
        my $ranking = $rankings[2];
        cmp-ok $ranking.rank, &[==], 3, 'correct rank';
        is-deeply $ranking.rankables, [1], 'correct rankable values';
    }
}

done-testing;
