use v6.c;
use Test;
use Ranker;

subtest 'when simple rankables are used' => sub {
    my @rankables = 1, 1, 2, 3, 3, 4, 5, 6, 7, 7, 7, 1, 1, 3;

    my $rankings = Ranker::rank(@rankables);

    is $rankings.elems, 7, 'correct number of rankings';

    subtest 'first ranking' => sub {
        my $ranking = $rankings[0];
        is $ranking.rank, 1, 'rank correct';
        is-deeply $ranking.rankables, [7, 7, 7], 'rankables correct';
    }

    subtest 'last ranking' => sub {
        my $ranking = $rankings[*-1];
        is $ranking.rank, 11, 'rank correct';
        is-deeply $ranking.rankables, [1, 1, 1, 1], 'rankables correct';
    }
}

class Player {
    has $.score;
}

subtest 'when ranking by block' => sub {
    my Player $player_1 .= new(score => 150);
    my Player $player_2 .= new(score => 100);
    my Player $player_3 .= new(score => 100);
    my Player $player_4 .= new(score => 25);

    my @players = $player_1, $player_2, $player_3, $player_4;
    my $rankings = Ranker::rank(@players, by => { $^player.score });

    is $rankings.elems, 3, 'correct number of rankings';

    subtest 'first ranking' => sub {
        my $ranking = $rankings[0];
        is $ranking.rank, 1, 'rank correct';
        is-deeply $ranking.rankables, [$player_1], 'rankables correct';
    }

    subtest 'second ranking' => sub {
        my $ranking = $rankings[1];
        is $ranking.rank, 2, 'rank correct';
        is-deeply $ranking.rankables, [$player_2, $player_3], 'rankables correct';
    }

    subtest 'third ranking' => sub {
        my $ranking = $rankings[2];
        is $ranking.rank, 4, 'rank correct';
        is-deeply $ranking.rankables, [$player_4], 'rankables correct';
    }
}

subtest 'when ranking by block and asc true' => sub {
    my Player $player_1 .= new(score => 150);
    my Player $player_2 .= new(score => 100);
    my Player $player_3 .= new(score => 100);
    my Player $player_4 .= new(score => 25);

    my @players = $player_1, $player_2, $player_3, $player_4;
    my $rankings = Ranker::rank(
        @players,
        by => { $^player.score },
        :asc
    );

    is $rankings.elems, 3, 'correct number of rankings';
    
    subtest 'first ranking' => sub {
        my $ranking = $rankings[0];
        is $ranking.rank, 1, 'rank correct';
        is-deeply $ranking.rankables, [$player_4], 'rankables correct';
    }

    subtest 'second ranking' => sub {
        my $ranking = $rankings[1];
        is $ranking.rank, 2, 'rank correct';
        is-deeply $ranking.rankables, [$player_2, $player_3], 'rankables correct';
    }

    subtest 'third ranking' => sub {
        my $ranking = $rankings[2];
        is $ranking.rank, 4, 'rank correct';
        is-deeply $ranking.rankables, [$player_1], 'rankables correct';
    }
}

done-testing;
